import React, { Component } from "react";

const Arrow = (props) => {
  return (
    <div
      style={{
        "margin-top":"30px",
        "margin-bottom":"10px",
        "text-align": "center",
        "cursor": "pointer"
      }}
    >
      <img src={props.icon} />
    </div>
  );
};

export default Arrow;
